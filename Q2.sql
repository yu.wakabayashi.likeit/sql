mysql> CREATE TABLE item (
    -> item_id int primary key,
    ->  item_name varchar(256) NOT NULL,
    -> item_price int NOT NULL,
    -> category_id int NOT NULL);
